package blog

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.assertj.core.api.Assertions.assertThat

@SpringBootTest
class BlogApplicationTests {
	@Test
	fun lixExtensionOnValidStringTest() {
		assertThat("Kotlin is a language that supports both imperative, functional and object oriented programming. The official site is full of documentation.".lix()).isEqualTo(50)
	}

	@Test
	fun lixExtensionOnInvalidStringTest() {
		assertThat("Kotlin is a language that supports both imperative, functional and object oriented programming The official site is full of documentation".lix()).isEqualTo(-1)
	}
}
