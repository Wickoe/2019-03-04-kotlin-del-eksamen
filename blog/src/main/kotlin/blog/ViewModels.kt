package blog

data class RenderedArticle(
        val slug: String,
        val title: String,
        val headline: String,
        val content: String,
        val author: User,
        val addedAt: String,
        val words: Int,
        val lix: Int)

data class TableViewModel(
        val tal: String,
        val takeNumbers: String)