package blog

fun tableSequence(table: Int) : List<Int> {
    return generateSequenceTable(table, 10)
}

fun generateSequenceTable(seed: Int, take: Int) : List<Int> {
    return generateSequence(seed){it + seed}.take(take).toList()
}