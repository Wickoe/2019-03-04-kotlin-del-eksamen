package blog

import java.time.LocalDateTime
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.*

fun LocalDateTime.format() = this.format(englishDateFormatter)

private val daysLookup = (1..31).associate { it.toLong() to getOrdinal(it) }

private val englishDateFormatter = DateTimeFormatterBuilder()
        .appendPattern("yyyy-MM-dd")
        .appendLiteral(" ")
        .appendText(ChronoField.DAY_OF_MONTH, daysLookup)
        .appendLiteral(" ")
        .appendPattern("yyyy")
        .toFormatter(Locale.ENGLISH)

private fun getOrdinal(n: Int) = when {
    n in 11..13 -> "${n}th"
    n % 10 == 1 -> "${n}st"
    n % 10 == 2 -> "${n}nd"
    n % 10 == 3 -> "${n}rd"
    else -> "${n}th"
}

fun String.toSlug() = toLowerCase()
        .replace("\n", " ")
        .replace("[^a-z\\d\\s]".toRegex(), " ")
        .split(" ")
        .joinToString("-")
        .replace("-+".toRegex(), "-")

fun String.countWords() : Int {
    return this.split(" ").count()
}

fun String.lix(): Int
{
    val dots = this.count { it == '.' }
    val wordsCount = this.countWords()

    if(dots <= 0 || wordsCount <= 0)
        return -1

    val longWords = this.split("[., ]".toRegex()).count { it.length > 6}

    return wordsCount / dots + longWords * 100 / wordsCount
}

fun Int.lixLevel() : String {
    return when(this){
        in 1..24 -> "Let tekst"
        in 25..34 -> "Under middel sværhedsgrad tekst"
        in 35..44 -> "Middel sværhedsgrad tekst"
        in 45..54 -> "Svær tekst"
        in 55..Int.MAX_VALUE -> "Meget svær tekst"
        else -> "Udefineret"
    }
}

fun Article.render() = RenderedArticle(
        slug,
        title,
        headline,
        content,
        author,
        addedAt.format(),
        words,
        lix)

