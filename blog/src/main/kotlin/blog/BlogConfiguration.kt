package blog

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BlogConfiguration {
    @Bean
    fun databaseInitializer(userRepository: UserRepository,
                            articleRepository: ArticleRepository) = ApplicationRunner {

        val smaldini = userRepository.save(User("smaldini", "Stéphane", "Maldini"))
        val maximus = userRepository.save(User("maxmus", "Mus", "Max"))
        userRepository.save(User("mimo","Mickey", "Duck"))

        articleRepository.save(Article(
                title = "Reactor Bismuth is out",
                headline = "Next generation core failure",
                content = "Extremity sweetness difficult behaviour he of. On disposal of as landlord horrible. Afraid at highly months do things on at.",
                author = smaldini
        ))

        articleRepository.save(Article(
                title = "Reactor Aluminium has landed",
                headline = "Lorem ipsum",
                content = "Ever man are put down his very. And marry may table him avoid. Hard sell it were into it upon. He forbade affixed parties of assured to me windows.",
                author = smaldini
        ))

        articleRepository.save(Article(
                title = "The matrix",
                headline = "Juno Reactor",
                content = "Fulfilled direction use continual set him propriety continued. Saw met applauded favourite deficient engrossed concealed and her.",
                author = maximus
        ))
    }
}