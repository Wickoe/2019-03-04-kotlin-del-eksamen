package blog

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class HtmlController(private val articleRepository: ArticleRepository,
                     private val userRepository: UserRepository) {

    @GetMapping("/")
    fun blog(model: Model): String {
        model["title"] = "Blog"
        model["articles"] = articleRepository.findAllByOrderByAddedAtDesc().map { it.render() }
        return "blog"
    }

    @GetMapping("/article/{slug}")
    fun article(@PathVariable slug: String, model: Model): String {
        val article = articleRepository
                .findBySlug(slug)
                ?.render()
                ?: throw IllegalArgumentException("Wrong article slug provided")

        model["title"] = article.title
        model["words"] = article.words
        model["lixLevel"] = article.lix.lixLevel()
        model["article"] = article

        return "article_new"
    }

    @GetMapping("/users")
    fun users(model: Model): String {
        model["title"] = "Users"

        model["users"] = userRepository.findAll().map {it }

        return "users"
    }

    @GetMapping("/table")
    fun table(model: Model) : String {
        model["title"] = "Table"
        return "table"
    }

    @PostMapping("/table")
    fun table(@ModelAttribute myform: TableViewModel) : ModelAndView {
        val mv = ModelAndView()

        mv.addObject("tal",myform.tal)
        mv.addObject("title", "Table")

        val tal = myform.tal.toInt()
        val takeNumbers = myform.takeNumbers.toInt()

        val table : List<Int>

        if(takeNumbers > 0)
            table = generateSequenceTable(tal, takeNumbers)
        else
            table = tableSequence(tal)

        mv.addObject("table", table.toString())

        mv.viewName = "table"

        return mv
    }
}